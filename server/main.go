package main

import (
	"bytes"
	"crypto/sha1"
	"encoding/hex"
	"fmt"
	"math/rand"
	"net"
	"strconv"
	"strings"
	"time"
)

func main() {
	fmt.Println("Starting the server in main.go...")

	quotes := []string{
		"Free software' is a matter of liberty, not price. To understand the concept, you should think of 'free' as in 'free speech,' not as in 'free beer' -- Richard Stallman",
		"There are no philosophical problems, there is only a suite of interconnected linguistic cul de sacs created by language's inability to reflect the truth -- Victor Pelevin",
		"I saw when the Lamb opened one of the seals, and I heard, as it were the noise of thunder, one of the four beasts saying, Come and see -- Revelation 6:1",
		"As you can see, we've had our eye on you for some time now, Mr. Anderson. It seems that you've been living two lives -- The Matrix",
	}

	secrets := []string{
		"aj4kh5j3hj",
		"bjshdu873d",
		"ckds878483",
		"djsdhfjhj4",
	}

	listen, err := net.Listen("tcp", ":8080")
	if err != nil {
		fmt.Println("ERROR", err)
		return
	}

	for {
		conn, err := listen.Accept()
		if err != nil {
			fmt.Println("ERROR", err)
			return
		}
		defer conn.Close()

		buf := make([]byte, 128)
		_, err = conn.Read(buf)
		if err != nil {
			fmt.Println("ERROR", err)
		}

		fmt.Println("======= buf:", buf)
		fmt.Println("======= string(buf):", string(buf))

		if buf[0] == 0 {
			// Fist request - asking for a challenge

			n := rand.Intn(len(quotes))
			conn.Write([]byte(secrets[n]))
		} else {
			// Verify
			resp := strings.Split(string(buf), ":")

			unix := resp[0]
			secret := resp[1]
			rnd := resp[2]
			hash := string(bytes.Trim([]byte(resp[3]), "\x00"))
			fmt.Printf("unix: %s, secret: %s, rnd: %s, hash: '%s'\n", unix, secret, rnd, hash)

			if hash[:3] != "000" {
				fmt.Println("ERROR hash prefix is invalid:", hash)
				continue
			}

			n, err := strconv.ParseInt(unix, 10, 64)
			if err != nil {
				fmt.Println("ERROR", err)
			}
			t := time.Unix(n, 0)
			sub := time.Now().Sub(t.Add(10 * time.Minute))
			if sub > 0 || sub < -10*time.Minute {
				fmt.Println("ERROR invalid difference:", sub)
				continue
			}

			hasher := sha1.New()
			forHash := unix + ":" + secret + ":" + rnd
			fmt.Println("====== forHash:", forHash)
			_n, err := hasher.Write([]byte(forHash))
			fmt.Println("==== wrote n:", _n)

			if err != nil {
				fmt.Println("ERROR", err)
			}

			fmt.Println("hasher.Sum(nil):", hasher.Sum(nil))
			_hash := hex.EncodeToString(hasher.Sum(nil))
			fmt.Printf("======= _hash): '%s'\n", _hash)
			fmt.Printf("===== []byte(hash) %v, []byte(_hash) %v\n", []byte(hash), []byte(_hash))
			if hash == _hash {
				i := index(secrets, secret)
				conn.Write([]byte(quotes[i]))
			} else {
				fmt.Println("ERROR: hash invalid")
			}
		}
	}
}

func index(s []string, v string) int {
	for i, _v := range s {
		if _v == v {
			return i
		}
	}

	return -1
}
