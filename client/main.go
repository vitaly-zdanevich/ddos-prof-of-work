package main

import (
	"context"
	"crypto/sha1"
	"encoding/hex"
	"fmt"
	"math"
	"net"
	"runtime"
	"strconv"
	"time"
)

func main() {
	fmt.Println("Working...")

	addr, err := net.ResolveTCPAddr("tcp", ":8080")
	if err != nil {
		fmt.Println("ERROR", err)
		return
	}

	conn, err := net.DialTCP("tcp", nil, addr)
	if err != nil {
		fmt.Println("ERROR", err)
		return
	}
	defer conn.Close()

	_, err = conn.Write([]byte{0})
	if err != nil {
		fmt.Println("ERROR", err)
		return
	}

	secret := make([]byte, 10)
	_, err = conn.Read(secret)
	if err != nil {
		fmt.Println("ERROR", err)
		return
	}

	ctx, cancel := context.WithCancel(context.Background())

	in := make(chan string, 12345678)
	found := make(chan string)

	go fill(in)
	for i := 0; i < runtime.NumCPU(); i++ {
		unix := strconv.FormatInt(time.Now().Unix(), 10)
		go worker(ctx, unix, string(secret), in, found)
	}

	f := <-found

	cancel()

	fmt.Println("====== FOUND: ", f)
	fmt.Println("====== FOUND bytes:", []byte(f))

	conn, err = net.DialTCP("tcp", nil, addr)
	if err != nil {
		fmt.Println("ERROR", err)
		return
	}
	defer conn.Close()

	_, err = conn.Write([]byte(f))
	if err != nil {
		fmt.Println("ERROR", err)
		return
	}

	quote := make([]byte, 1024)
	_, err = conn.Read(quote)
	if err != nil {
		fmt.Println("ERROR", err)
		return
	}
	fmt.Println(string(quote))

}

func fill(in chan<- string) {
	for i := 0; i < math.MaxInt; i++ {
		in <- strconv.Itoa(i)
	}

}

func worker(ctx context.Context, unix, secret string, in <-chan string, found chan<- string) {
	for {
		select {
		case <-ctx.Done():
			{
				return
			}
		default:

			hasher := sha1.New()

			i := <-in

			forHash := unix + ":" + secret + ":" + i
			// fmt.Println("====== forHash:", forHash)
			n, err := hasher.Write([]byte(forHash))
			if err != nil {
				fmt.Println("ERROR on hasher.Write():", err)
				return
			}
			hash := hasher.Sum(nil)
			if isValid(hash) {
				found <- unix + ":" + secret + ":" + i + ":" + hex.EncodeToString(hash)
				fmt.Println("============ []byte(secret):", []byte(secret))
				fmt.Println("============ []byte(i):", []byte(i))
				fmt.Println("==== wrote n:", n)
				return
			}
		}
	}
}

// Hashcash
func isValid(sl []byte) bool {
	for _, v := range sl[:3] {
		if v != 0 {
			return false
		}
	}

	return true
}
